--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.13
-- Dumped by pg_dump version 9.6.13

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO gabriel;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO gabriel;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO gabriel;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO gabriel;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO gabriel;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO gabriel;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO gabriel;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO gabriel;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO gabriel;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO gabriel;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO gabriel;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO gabriel;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO gabriel;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO gabriel;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO gabriel;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO gabriel;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO gabriel;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO gabriel;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO gabriel;

--
-- Name: propiedades_faq; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.propiedades_faq (
    id integer NOT NULL,
    pregunta text NOT NULL,
    respuesta text NOT NULL
);


ALTER TABLE public.propiedades_faq OWNER TO gabriel;

--
-- Name: propiedades_faq_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.propiedades_faq_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.propiedades_faq_id_seq OWNER TO gabriel;

--
-- Name: propiedades_faq_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.propiedades_faq_id_seq OWNED BY public.propiedades_faq.id;


--
-- Name: propiedades_propiedad; Type: TABLE; Schema: public; Owner: gabriel
--

CREATE TABLE public.propiedades_propiedad (
    id integer NOT NULL,
    barrio character varying(50) NOT NULL,
    calle character varying(50) NOT NULL,
    cuidad character varying(50) NOT NULL,
    estado character varying(50) NOT NULL,
    image character varying(500) NOT NULL,
    latitud character varying(100) NOT NULL,
    nro character varying(5) NOT NULL,
    precio integer NOT NULL,
    publicar integer NOT NULL,
    descripcion text NOT NULL,
    dormitorios character varying(5) NOT NULL,
    id_tipo_moneda integer NOT NULL,
    tipo_operacion character varying(15) NOT NULL,
    tipo_propiedad character varying(15) NOT NULL
);


ALTER TABLE public.propiedades_propiedad OWNER TO gabriel;

--
-- Name: propiedades_propiedad_id_seq; Type: SEQUENCE; Schema: public; Owner: gabriel
--

CREATE SEQUENCE public.propiedades_propiedad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.propiedades_propiedad_id_seq OWNER TO gabriel;

--
-- Name: propiedades_propiedad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: gabriel
--

ALTER SEQUENCE public.propiedades_propiedad_id_seq OWNED BY public.propiedades_propiedad.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: propiedades_faq id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.propiedades_faq ALTER COLUMN id SET DEFAULT nextval('public.propiedades_faq_id_seq'::regclass);


--
-- Name: propiedades_propiedad id; Type: DEFAULT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.propiedades_propiedad ALTER COLUMN id SET DEFAULT nextval('public.propiedades_propiedad_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add propiedad	1	add_propiedad
2	Can change propiedad	1	change_propiedad
3	Can delete propiedad	1	delete_propiedad
4	Can view propiedad	1	view_propiedad
5	Can add faq	2	add_faq
6	Can change faq	2	change_faq
7	Can delete faq	2	delete_faq
8	Can view faq	2	view_faq
9	Can add log entry	3	add_logentry
10	Can change log entry	3	change_logentry
11	Can delete log entry	3	delete_logentry
12	Can view log entry	3	view_logentry
13	Can add permission	4	add_permission
14	Can change permission	4	change_permission
15	Can delete permission	4	delete_permission
16	Can view permission	4	view_permission
17	Can add group	5	add_group
18	Can change group	5	change_group
19	Can delete group	5	delete_group
20	Can view group	5	view_group
21	Can add user	6	add_user
22	Can change user	6	change_user
23	Can delete user	6	delete_user
24	Can view user	6	view_user
25	Can add content type	7	add_contenttype
26	Can change content type	7	change_contenttype
27	Can delete content type	7	delete_contenttype
28	Can view content type	7	view_contenttype
29	Can add session	8	add_session
30	Can change session	8	change_session
31	Can delete session	8	delete_session
32	Can view session	8	view_session
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 32, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$150000$Xk6WCGYChZVG$oThgYBknXwZUNXjSE1+JnY9c8MBXOHDFH2hmaoR45x4=	2019-06-05 15:23:58.772412+00	t	gabriel			acl.gabriel@gmail.com	t	t	2019-06-05 11:56:30.258717+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2019-06-05 15:31:35.10395+00	7	Faq object (7)	1	[{"added": {}}]	2	1
2	2019-06-05 15:50:58.688134+00	4	Propiedad object (4)	2	[{"changed": {"fields": ["barrio"]}}]	1	1
3	2019-06-05 15:51:11.808324+00	4	Propiedad object (4)	2	[{"changed": {"fields": ["barrio"]}}]	1	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 3, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	propiedades	propiedad
2	propiedades	faq
3	admin	logentry
4	auth	permission
5	auth	group
6	auth	user
7	contenttypes	contenttype
8	sessions	session
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 8, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2019-06-05 11:55:37.445581+00
2	auth	0001_initial	2019-06-05 11:55:37.531783+00
3	admin	0001_initial	2019-06-05 11:55:37.69608+00
4	admin	0002_logentry_remove_auto_add	2019-06-05 11:55:37.730242+00
5	admin	0003_logentry_add_action_flag_choices	2019-06-05 11:55:37.743226+00
6	contenttypes	0002_remove_content_type_name	2019-06-05 11:55:37.769947+00
7	auth	0002_alter_permission_name_max_length	2019-06-05 11:55:37.778572+00
8	auth	0003_alter_user_email_max_length	2019-06-05 11:55:37.791237+00
9	auth	0004_alter_user_username_opts	2019-06-05 11:55:37.805074+00
10	auth	0005_alter_user_last_login_null	2019-06-05 11:55:37.81851+00
11	auth	0006_require_contenttypes_0002	2019-06-05 11:55:37.821817+00
12	auth	0007_alter_validators_add_error_messages	2019-06-05 11:55:37.834764+00
13	auth	0008_alter_user_username_max_length	2019-06-05 11:55:37.853492+00
14	auth	0009_alter_user_last_name_max_length	2019-06-05 11:55:37.870275+00
15	auth	0010_alter_group_name_max_length	2019-06-05 11:55:37.88382+00
16	auth	0011_update_proxy_permissions	2019-06-05 11:55:37.895962+00
17	propiedades	0001_initial	2019-06-05 11:55:37.917293+00
18	propiedades	0002_faq	2019-06-05 11:55:37.940733+00
19	sessions	0001_initial	2019-06-05 11:55:37.963965+00
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 19, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
41ow86zo0fia31xuc9413j8yqdey0wnw	YzhhYmY5NWNkMmM5NjhjYjA3ZWMzMzA5NWNiM2IyZDZmY2RhZmE1Nzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiIzMjQzYmVjNzc3NTA4OGI0MzQ1MzE2MDNkODcyZGEzNGJjZGRmMDU0In0=	2019-06-19 15:23:58.776034+00
\.


--
-- Data for Name: propiedades_faq; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.propiedades_faq (id, pregunta, respuesta) FROM stdin;
2	¿Qué tiempo normalmente se conviene entre la firma del Boleto de Compra Venta y la Escritura?	El plazo lo estipulan entre las partes de acuerdo a sus necesidades. Normalmente  no menos de 30 días corridos.
3	¿Quién designa al escribano que realizará la operatoria?	Normalmente el escribano selecciona el comprador en el Boleto de Compra Venta. Hay excepciones, si la compra es con financiación, en esos casos al escribano suele ser el designado acreedor hipotecario. Pasa lo mismo cuando se trata de una "primera Escritura", es decir un edificio "a estrenar" o un nuevo loteo.
4	¿Que documentación se suscribe hasta la Escritura?	Generalmente son tres los documentos: Reserva,  Boleto - Venta y Escritura traslativa  de dominio.
5	¿Qué es la Reserva de Compra?	Este  documento simplemente es una "oferta" que el oferente realiza por la propiedad en cuestión depositando una suma de dinero, y  que en tanto no sea aceptada  por la vendedora, la operación no estará cerrada.
6	¿Para que sirve la reserva?	Abre una instancia de negociación seria entre las partes, en algunos casos se puede realizar una seña una vez conformada la oferta, incluyendo un pacto comisorio en la misma, Independientemente de eso una vez que la propiedad es reservada, el inmueble  es retirado de la venta y se comienzan los trámites  preparatorios para la suscripción del Boleto de Compra Venta o de la Escritura directa (siempre posterior a la aceptación
7	asas	asasa
\.


--
-- Name: propiedades_faq_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.propiedades_faq_id_seq', 7, true);


--
-- Data for Name: propiedades_propiedad; Type: TABLE DATA; Schema: public; Owner: gabriel
--

COPY public.propiedades_propiedad (id, barrio, calle, cuidad, estado, image, latitud, nro, precio, publicar, descripcion, dormitorios, id_tipo_moneda, tipo_operacion, tipo_propiedad) FROM stdin;
1	Palermo	Aguero	Buenos Aires	bueno	https://firebasestorage.googleapis.com/v0/b/acl-api-1992.appspot.com/o/12chacabuco.jpg?alt=media&token=2b035b32-7d95-4c18-a4ec-c33008046ed9	-34.443732, -58.546056	1580	145000	1	Casa estilo "Colonial" en lote 8.66 x 24 living comedor amplio,dos dormitorios de 5.50 x 5.50 cada uno,cocina 4 x 4, baño,zaguán,patio y parrilla.	2	1	Venta	Casa
2	Recoleta	Av. Santa Fe	Buenos Aires	bueno	https://firebasestorage.googleapis.com/v0/b/acl-api-1992.appspot.com/o/casa_new_(8).jpg?alt=media&token=3d27f16b-d1ff-4d6e-933e-8378ca838ae5	-34.443732, -58.546056	2050	25000	1	Impecable departamento de tres ambientes con excelente vista abierta. Dos dormitorios con placard con interiores Cocina con alacenas y bajo mesada con lavadero incorporado BaÃ±o completo	2	2	Alquiler	Departamento
3	Chacarita	Av. Corrientes	Buenos Aires	bueno	https://firebasestorage.googleapis.com/v0/b/acl-api-1992.appspot.com/o/14-padre_acevedo.jpg?alt=media&token=e122adb0-e417-4020-a28d-83075aaa9d06	-34.443732, -58.546056	5050	250000	1	Dos casas sobre lote de 10 x 30 cada una con entrada independienteCasa  al frente :Dos dormitorios living comedor,cocina completa y baño entrada de autos.Casa al fondo PB:cocina comedor diario en la planta baja con buenas aberturas de madera.PA:tres dormitorios con placard baño completo.La casa está en muy buen estado.Galpón,Amplio jardín y pileta	2	1	Venta	Casa
5	Colegiales	Cramer	Buenos Aires	bueno	https://firebasestorage.googleapis.com/v0/b/acl-api-1992.appspot.com/o/img-20150123-wa0009.jpg?alt=media&token=059af0dc-8bc6-40cd-a1c4-9635924dfa92	-34.443732, -58.546056	650	14000	1	Hermoso departamento frente a la estación de Victoria,Living comedor con pisos de cerámicos con cocina integrada con alacenas y bajo mesada,dos dormitorios con placard y estufas tiro balanceado,baño completo.	2	1	Alquiler	Departamento
6	Villa Urquiza	Congreso	Buenos Aires	bueno	https://firebasestorage.googleapis.com/v0/b/acl-api-1992.appspot.com/o/20151006_124801.jpg?alt=media&token=5dfa8a85-8e4b-4149-9f82-e6b30f291ed3	-34.443732, -58.546056	1650	16000	1	Excelente dos ambientes a estrenar en el edificio "Palacios",living comedor  amplio muy luminoso cocina integrada con barra desayunadora,con alacenas y bajo mesada,caldera,toilette,1 dormitorio con placard con interiores baño en suite,balcón con parrilla.Gimnasio,piscina	2	1	Alquiler	Departamento
4	Belgrano	Juramento 1775	Buenos Aires	bueno	https://firebasestorage.googleapis.com/v0/b/acl-api-1992.appspot.com/o/20120711_112124(2).jpg?alt=media&token=a0f7cd5a-6707-44fb-97f6-a3f26e97b89a	-34.443732, -58.546056	5050	120000	1	Excelente departamento a estrenar muy luminoso sobre importante avenida. Cocina comedor con barra desayunadora, pisos de cer&aacute;¡micos. Las aberturas son de aluminio con doble vidrio.  1 Dormitorio con placard con pisos de cer&aacute;¡micos. Baño completo. Cochera .	1	1	Venta	Departamento
\.


--
-- Name: propiedades_propiedad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: gabriel
--

SELECT pg_catalog.setval('public.propiedades_propiedad_id_seq', 6, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: propiedades_faq propiedades_faq_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.propiedades_faq
    ADD CONSTRAINT propiedades_faq_pkey PRIMARY KEY (id);


--
-- Name: propiedades_propiedad propiedades_propiedad_pkey; Type: CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.propiedades_propiedad
    ADD CONSTRAINT propiedades_propiedad_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: gabriel
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: gabriel
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

