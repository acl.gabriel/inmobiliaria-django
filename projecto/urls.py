from django.contrib import admin
from django.urls import path
from propiedades import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index),
    path('propiedad/<int:id>/', views.propiedad),
    path('empresa/', views.empresa),
    path('faq/', views.faqs)
]
