from django.contrib import admin
from .models import Propiedad, Faq

# Register your models here.

@admin.register(Propiedad)
class PropiedadAdmin(admin.ModelAdmin):
    list_display = ('cuidad', 'barrio', 'precio', 'tipo_propiedad','tipo_operacion')
    list_filter = ('barrio','tipo_propiedad', 'tipo_operacion')

@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
    list_display = ('pregunta','respuesta')
