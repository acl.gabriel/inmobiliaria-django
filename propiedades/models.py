from django.db import models

class Propiedad(models.Model):
    barrio  = models.CharField(max_length=50)
    calle   = models.CharField(max_length=50)
    cuidad  = models.CharField(max_length=50)
    estado  = models.CharField(max_length=50)
    image   = models.CharField(max_length=500)
    latitud = models.CharField(max_length=100)
    nro     = models.CharField(max_length=5)
    precio  = models.IntegerField()
    publicar    = models.IntegerField()
    descripcion = models.TextField()
    dormitorios = models.CharField(max_length=5)
    id_tipo_moneda  = models.IntegerField()
    tipo_operacion  = models.CharField(max_length=15)
    tipo_propiedad  = models.CharField(max_length=15)

class Faq(models.Model):
    pregunta = models.TextField()
    respuesta = models.TextField()
