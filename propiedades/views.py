from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .models import Faq,Propiedad


def index(request):
    Propiedades = Propiedad.objects.all()
    return render(request, 'index.html', dict(Propiedades = Propiedades))

def propiedad(request, id):
    return HttpResponse('<H2>Propiedad</H2>')

def empresa(request):
    # Propiedad.objects.create(
    #     barrio  = 'Villa Urquiza',
    #     calle   = 'Congreso',
    #     cuidad  = 'Buenos Aires',
    #     estado  = 'bueno',
    #     image   = 'https://firebasestorage.googleapis.com/v0/b/acl-api-1992.appspot.com/o/20151006_124801.jpg?alt=media&token=5dfa8a85-8e4b-4149-9f82-e6b30f291ed3',
    #     latitud = '-34.443732, -58.546056',
    #     nro     = '1650',
    #     precio  =   '16000',
    #     publicar    = '1',
    #     descripcion = 'Excelente dos ambientes a estrenar en el edificio "Palacios",living comedor  amplio muy luminoso cocina integrada con barra desayunadora,con alacenas y bajo mesada,caldera,toilette,1 dormitorio con placard con interiores baño en suite,balcón con parrilla.Gimnasio,piscina',
    #     dormitorios = '2',
    #     id_tipo_moneda  = '1',
    #     tipo_operacion  = 'Alquiler',
    #     tipo_propiedad  = 'Departamento'
    # )
    return render(request, 'empresa.html')

def faqs(request):
    #Faq.objects.create(pregunta='')
    #return JsonResponse(dict(data = data))
    faqs = Faq.objects.all()
    return render(request, 'faqs.html', dict(faqs = faqs))
    
